#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <pthread.h>  
#include <sys/time.h>
#include <semaphore.h> 

int sockfd, t_pending, done, num_vars,operation_count,recv_seq_no,cc_done,total_num_aborts;

int rand_sleep = 0;

struct timeval total_t,total_abort_t,non_abort_t,op_start,abort_start,tot_start, tot_end, xact_start, xact_finish;
unsigned long total_t_l,total_abort_t_l,non_abort_t_l,op_start_l,abort_start_l,tot_start_l, tot_end_l;

int siteno;
int num_transactions;
int aborttx;
int fromfile=0;
char * fname;
FILE * fp,*infile;

pthread_t cc_read;
sem_t all_released;

char * vars;

char * rvar;
char * rsleep;
char * top;
int * operand2;
int * var_val;
int * lock;
int lock_count = 0;
int completed = 0;

void * perform_op(int index){
	int varindex = ((int)rvar[index]) - 97;

	int theval;

	fprintf(fp, "%c = %c %c %d; ",rvar[index],rvar[index],top[index],operand2[index]);

	if( top[index] == '+' ){
		theval = var_val[varindex] + operand2[index];
	}else if( top[index] == '-' ){
		theval = var_val[varindex] - operand2[index];
	}else if( top[index] == '*' ){
		theval = var_val[varindex] * operand2[index];
	}
	fprintf(fp,"\n");

	char sendbuf[7];
	sendbuf[0] = 'O';
	sendbuf[1] = vars[varindex];
	sendbuf[2] = theval;
	sendbuf[3] = theval >> 8;
	sendbuf[4] = theval >> 16;
	sendbuf[5] = theval >> 24;					
	sendbuf[6] = '\0';
	write(sockfd,&sendbuf[0], 7);

}

void * read_cc(){
	int n,i,j;	
	char buffer[1500],tbuf[1500];
	while( !cc_done ){
		//actually we need to be very careful here when we receive an update from the CC
		//we need to keep an update sequence counter so that the updates are synchronized.
		//fprintf(stderr,"blocked\n");
		n = read(sockfd,tbuf,1500);
		j=0;

		//fprintf(stderr,"read %d bytes\n",n);
		while( j < n ){
			//fprintf(stderr,"read index %d total %d %c\n",j,n,tbuf[j]);
			memcpy(&buffer[0],&tbuf[j],1500-j);
			//fprintf(stderr,"read %s ",buffer);
			//printf("read length %d index %d\n",n,j);
			if( buffer[0] == 'G' ){
				//lock has been granted
				int index = ((int)buffer[1]) - 97;

				struct timeval tv;
				gettimeofday(&tv,NULL);
				time_t nowtime;
				struct tm *nowtm;
				char tbuf[64], tbuf2[64];

				gettimeofday(&tv, NULL);
				nowtime = tv.tv_sec;
				nowtm = localtime(&nowtime);

				strftime(&tbuf[0],sizeof(tbuf), "%H:%M:%S",nowtm);
				snprintf(&tbuf2[0],sizeof(tbuf2), "%s.%06d", tbuf, tv.tv_usec);

				fprintf(fp, "Lock %c granted for Transaction T%d at time %s\n",vars[index],completed,tbuf2);
				fprintf(stderr,"Granting lock on %c %d %d %d\n",vars[index],lock_count,operation_count,lock[index]);
				if( lock[index] == 0 ){			
					lock[index] = 1;
					lock_count++;
				}

				//all locks have been granted. Start performing ops
				if( lock_count == operation_count ){
					fprintf(stderr,"All locks granted. Performing ops\n");
					//start performing ops and informing CC


					//fprintf(stderr,"All locks granted. Performing ops\n");
					fprintf(fp, "Performing Operations for Transaction T%d at time %s\n",completed,tbuf2);

					for(i=0; i< operation_count; i++ ){
						perform_op(i);
					}

					aborttx = 0;
				
				}
				j+=3;		
			}else if( buffer[0] == 'K' ){
				//release the lock
				int index = ((int)buffer[1]) - 97;
				struct timeval tv;
				gettimeofday(&tv,NULL);
				time_t nowtime;
				struct tm *nowtm;
				char tbuf[64], tbuf2[64];

				gettimeofday(&tv, NULL);
				nowtime = tv.tv_sec;
				nowtm = localtime(&nowtime);

				strftime(&tbuf[0],sizeof(tbuf), "%H:%M:%S",nowtm);
				snprintf(&tbuf2[0],sizeof(tbuf2), "%s.%06d", tbuf, tv.tv_usec);

				fprintf(stderr,"CC released lock %c at time %s\n",vars[index],tbuf2);

				fprintf(fp, "Lock %c released for Transaction T%d at time %s\n",vars[index],completed,tbuf2);
				if( lock[index] == 1 ){
					lock[index] = 0;
					lock_count--;
				}		

				if( lock_count == 0 ){
					sem_post(&all_released);
				}
				j+= 3;

			}else if( buffer[0] == 'U' ){
				//make sure we have correct sequence number and update

				struct timeval tv;
				gettimeofday(&tv,NULL);
				time_t nowtime;
				struct tm *nowtm;
				char tbuf[64], tbuf2[64];

				gettimeofday(&tv, NULL);
				nowtime = tv.tv_sec;
				nowtm = localtime(&nowtime);

				strftime(&tbuf[0],sizeof(tbuf), "%H:%M:%S",nowtm);
				snprintf(&tbuf2[0],sizeof(tbuf2), "%s.%06d", tbuf, tv.tv_usec);

				int *tempi = &buffer[6];
				if( recv_seq_no == *tempi ){
					char tempc = buffer[1];
					int varindex = ((int)tempc) - 97;
					int * copier = &buffer[2];
					var_val[varindex] = *copier;
					fprintf(stderr,"UPDATE of %c at index %d value %d\n",vars[varindex],varindex,*copier);
					fprintf(fp,"Received copier transaction %c = %d with sequence #%d\n",vars[varindex],*copier,recv_seq_no);
					recv_seq_no++;				
				}else{
					fprintf(stderr,"ERROR UPDATE OUT OF SEQ! %d %d\n",recv_seq_no,*tempi);
				}
				
				memset(buffer,'\0',100);
				j += 11;
			
			}else if( buffer[0] == 'A' ){
				fprintf(stderr,"Abort received!\n");
				aborttx = 1;

				int index = ((int)buffer[1]) - 97;
				struct timeval tv;
				gettimeofday(&tv,NULL);
				time_t nowtime;
				struct tm *nowtm;
				char tbuf[64], tbuf2[64];

				gettimeofday(&tv, NULL);
				nowtime = tv.tv_sec;
				nowtm = localtime(&nowtime);

				strftime(&tbuf[0],sizeof(tbuf), "%H:%M:%S",nowtm);
				snprintf(&tbuf2[0],sizeof(tbuf2), "%s.%06d", tbuf, tv.tv_usec);


				fprintf(fp, "Abort received for Transaction T%d at time %s\n",completed,tbuf2);

				char buf1[2];
				buf1[0] = 'L';
				buf1[1] = '\0';
				write(sockfd,buf1, 2);

				//realease all locks
				for(i=0; i < num_vars; i++){
					lock[i] = 0;
				}
				
				lock_count = 0;	
				sem_post(&all_released);					
				j+=2;
				//fprintf(stderr,"-----------j %d n %d\n",j,n);
			}else if( buffer[0] == 'D' ){
				cc_done = 1;
				j+=2;
			}
			//fprintf(stderr,"mem1\n");
			memset(buffer,'\0',1500);
			//fprintf(stderr,"mem2\n");
		}
		memset(tbuf,'\0',1500);
		//fprintf(stderr,"continue read\n");
	}
}

void error(const char *msg)
{
    perror(msg);
    exit(-1);
}

void * generate_transaction(){
	int i,j,n,resendtx;
	resendtx = 0;


	gettimeofday(&tot_start, NULL);
	while( !done ){
		aborttx = 0;

		//for each var assign it randomly into the transaction chain
		if(!resendtx){
			int insert,r;
			int opindex = 0;
			gettimeofday(&xact_start,NULL);
			if( fromfile ){
				char line[100];
				if( fgets(line, sizeof(line), infile) ){
					for(j=0;j<operation_count;j++){
						rvar[j] = line[(4*j)];
						top[j] = line[1+(4*j)];
						operand2[j] = line[2+(4*j)] - '0';
						rsleep[j] = line[3+(4*j)];
					}

				}else{
					gettimeofday(&tot_end, NULL);
					return;
				}
			}else{

				while( opindex < operation_count ){

					insert = 1;
					r = (rand()+r) % num_vars;
					for(j=0;j<operation_count;j++){
						//fprintf(stderr,"%c-%c %d %d %d\n",rvar[j],vars[r],j,r, num_vars);
						if( rvar[j] == vars[r] ){
							insert = 0;
							break;
						}
					}
					if( insert ){
						//fprintf(stderr,"---R %d\n",r);
						rvar[opindex] = vars[r];
						operand2[opindex] = rand() % 4;
						switch( rand() % 3 ){
							case 0:
								top[opindex] = '+';
								break;
							case 1:
								top[opindex] = '-';
								break;
							case 2:
								top[opindex] = '*';
								break;
							default:
								top[opindex] = '+';
								break;
						}				
						opindex++;
					}
				}
			}

			time_t nowtime;
			struct tm *nowtm;
			char tbuf[64], tbuf2[64];

			nowtime = xact_start.tv_sec;
			nowtm = localtime(&nowtime);

			strftime(&tbuf[0],sizeof(tbuf), "%H:%M:%S",nowtm);
			snprintf(&tbuf2[0],sizeof(tbuf2), "%s.%06d", tbuf, xact_start.tv_usec);

			fprintf(fp, "Transaction T%d started at %s.\n",completed,tbuf2);
			for(i=0;i<operation_count;i++){
				fprintf(fp, "%c%c%d ",rvar[i],top[i],operand2[i]);
			}
			fprintf(fp, "\n");
		}

		//okay transaction has been generated
		//now send to CC to aquire locks
		
		char buf[5];
		gettimeofday(&op_start, NULL);
		for(i=0; i< operation_count; i++ ){
			buf[0] = 'R';
			buf[1] = rvar[i];
			buf[2] = top[i];
			buf[3] = (char)operand2[i] + '0';
			buf[4] = '\0';
			fprintf(stderr,"requesting %s resend %d abort %d\n",buf,resendtx,aborttx);
			if( aborttx == 1 ){
				resendtx = 1;
				break;
			}else{
				n = write(sockfd,buf, 5);
			}
			//this usleep is here to space out the lock requestes to force deadlocks
			if( fromfile ){
				sleep(rsleep[i] - '0');
			}else{
				if( rand_sleep ){
					int sleep = (rand() % 500000)+ 30000;	
	
					usleep(sleep);
				}
			}
		}


		sem_wait(&all_released);
		if( aborttx == 1 ){
			total_num_aborts++;
			resendtx = 1;
			struct timeval stop;
			gettimeofday(&stop, NULL);
			total_abort_t_l += (unsigned long)((stop.tv_sec * 1000000) + (stop.tv_usec)) - (unsigned long)((op_start.tv_sec * 1000000) + (op_start.tv_usec));
			continue;
		}else{
			struct timeval stop;
			gettimeofday(&stop, NULL);
			non_abort_t_l += (unsigned long)((stop.tv_sec * 1000000) + (stop.tv_usec)) - (unsigned long)((op_start.tv_sec * 1000000) + (op_start.tv_usec));
			resendtx = 0;
			fprintf(stderr,"All locks have been released!!\n");
			for(i=0;i<operation_count;i++){
				rvar[i] = NULL;			
			}

			time_t nowtime;
			struct tm *nowtm;
			char tbuf[64], tbuf2[64];

			nowtime = stop.tv_sec;
			nowtm = localtime(&nowtime);

			strftime(&tbuf[0],sizeof(tbuf), "%H:%M:%S",nowtm);
			snprintf(&tbuf2[0],sizeof(tbuf2), "%s.%06d", tbuf, stop.tv_usec);

			fprintf(fp, "Transaction T%d completed at %s.\n",completed,tbuf2);

			completed++;

		}

		if(num_transactions == completed){
			done = 1;
		}
	}
	gettimeofday(&tot_end, NULL);
}

int main(int argc, char *argv[])
{
	int portno, n;
	struct sockaddr_in serv_addr;
	struct hostent *server;
	cc_done = 0;

	t_pending = 0;
	done = 0;	

	if (argc < 5) {
		fprintf(stderr,"usage %s <hostname> <num transactions> <num vars per transaction> <1/0random sleep between lock requests> <optional filename>\n", argv[0]);
		exit(0);
	}

	if( argc == 6 ){
		infile = fopen(argv[5], "r");
		fromfile = 1;
	}

	rand_sleep = atoi(argv[4]);
	sem_init(&all_released, 1, 0); 

	recv_seq_no = 1;
	portno = 51234;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0){ 
       		error("ERROR opening socket");
	}

	total_num_aborts = 0;
	total_t_l = 0;
	total_abort_t_l = 0;
	non_abort_t_l = 0;
	op_start_l = 0;
	abort_start_l = 0;
	tot_start_l = 0;
	tot_end_l = 0;

	operation_count = atoi(argv[3]);
	num_transactions = atoi(argv[2]);
	server = gethostbyname(argv[1]);
	if (server == NULL) {
        	fprintf(stderr,"ERROR, no such host\n");
        	exit(0);
	}

	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
	serv_addr.sin_port = htons(portno);
	if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0){ 
		error("ERROR connecting");
	}

	//receive the number of variables the CC is requesting for this demo
    	char buffer[100];
	n = read(sockfd,buffer,100);
	if( buffer[0] == 'N' ){
		//char temp = buffer[1];
		int *temp = &buffer[1];	
		num_vars = *temp;
		fprintf(stderr,"Site online, CC requested %d variables!\n",num_vars);
		vars = (char *) malloc(num_vars);
		rvar  = (char *) malloc(operation_count);
		rsleep  = (char *) malloc(operation_count);
		top  = (char *) malloc(operation_count);
		operand2 = (int *) malloc(sizeof(int)*operation_count);
		var_val = (int *) malloc(sizeof(int)*num_vars);
		lock = (int *) malloc(sizeof(int)*num_vars);

		int i;
		for(i=0; i < num_vars; i++ ){
			vars[i] = (char)97+i;
			var_val[i] = 1;
			lock[i] = 0;
		}
		for(i=0; i < operation_count; i++ ){
			rvar[i] = NULL;
		}
	}

	siteno = atoi(&buffer[6]);
	fname = &buffer[6];
	fp = fopen(fname,"w+");

	srand(time(NULL) *(siteno+17));

	fprintf(stderr,"Spawning read thread\n");
	
	if( pthread_create( &cc_read, NULL, &read_cc, NULL) ){
		error("ERROR cannot create read thread for site 1!");
	}

	fprintf(stderr,"Generating transactions!\n");
	generate_transaction();

	char buf[2];
	buf[0] = 'D';
	buf[1] = '\0';
	write(sockfd,buf,2);

	while(!cc_done){
		sleep(1);
	}

	fprintf(fp,"\n");
	int i;
	for(i=0;i<num_vars;i++){
		fprintf(fp, "%c:%d ",vars[i],var_val[i]);
	}
	fprintf(fp,"\n");
	fprintf(fp,"Total number of aborts: %d\n",total_num_aborts);
	fprintf(fp,"Time spent sending aborted requests: %ldms\n",total_abort_t_l);
	fprintf(fp,"Time spent sending completed requests: %ldms\n",non_abort_t_l);
	fprintf(fp,"Total time spent: %ldms\n",(unsigned long)(tot_end.tv_sec*1000000 + tot_end.tv_usec) - (unsigned long)(tot_start.tv_sec*1000000 + tot_start.tv_usec));


	fclose(fp);
	close(sockfd);
	if( argc == 6 ){
		fclose(infile);
	}
}
