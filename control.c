#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <time.h> 
#include <sys/queue.h>
#include <semaphore.h> 

pthread_t *site_read;

sem_t mutex, start_trans;

int *sitefd, num_vars, update_seq_no, num_sites, *need_abort, *site_done, *abort_sent;

char * vars;

struct lockq {
	int site;
	struct lockq *next;
} **locks, **ltails;

struct x_graph {
	int visited;
	struct x_graph **conflict;
} **site_graph;

int check_hold_lock(int lindex,int site){
	struct lockq * temp = locks[lindex];
	while( temp != NULL ){
		if( temp->site == site ){
			//fprintf(stderr,"Site %d already holding %c", site,(char)lindex + 97);
			return 1;
		}
		temp = temp->next;
	}
	return 0;
}

void * dfs_graph(struct x_graph *node,int sindex){
	int i;

	visit_node(node,sindex);
	//fprintf(stderr,"-ERROR! cycle detected in graph! Node already visited! Sending abort to %d\n",sindex);
		
	for(i=0; i <num_sites; i++ ){
		site_graph[i]->visited = 0;
	}
}

void * release_all_site_locks(int site){
	int i,j;
	fprintf(stderr,"Releaseing locks for site %d ",site);
	
	for(i=0; i < num_vars; i++ ){
		struct lockq * temp = locks[i];
		struct lockq * prev = NULL;
		struct lockq * tofree = NULL;
		while( temp != NULL ){
			if( temp->site == site ){
				fprintf(stderr," holding %c", (char)i + 97);
				if( prev != NULL ){
					if( temp->next != NULL ){
						prev->next = temp->next;
					}else{
						prev->next = NULL;
						ltails[i] = prev;
					}
				}else{
					if( temp->next != NULL ){

						locks[i] = temp->next;
						char grant[3];
						grant[0] = 'G';
						grant[1] = vars[i];
						grant[2] = '\0';
						fprintf(stderr," granting lock %c to site %d, ",vars[i],locks[i]->site);	

						write(sitefd[locks[i]->site],grant, 3);

						/*if( locks[i]->next != NULL ){
							site_graph[locks[i]->next->site]->conflict[locks[i]->site] = site_graph[locks[i]->site];
							dfs_graph(site_graph[locks[i]->next->site],locks[i]->next->site);
							if( need_abort[locks[i]->next->site] ){
								char abort[2];
								abort[0] = 'A';
								abort[1] = '\0';
								//fprintf(stderr,"---------------------write the abort!\n");	
								write(sitefd[j],abort, 2);
								release_all_site_locks(locks[i]->next->site);
							}
						}*/
								
					}else{
						locks[i] = NULL;
						ltails[i] = NULL;
					}
				}
				tofree = temp;
				//break;
			}
			prev = temp;
			temp = temp->next;
			if( tofree != NULL ){
				free(tofree);
				tofree = NULL;
			}
		}
	}

	rehash_conflicts();

	

	fprintf(stderr,"done release!\n");
}

//TODO make a check_release function to check if we should have a conflict after a single lock release...
int rehash_after_single_release(int site){
	int i;
	for(i=0;i<num_sites;i++){
		site_graph[site]->conflict[i] = NULL;
	}	

	for(i=0;i<num_vars;i++){
		if( locks[i] != NULL && locks[i]->next != NULL ){
			site_graph[locks[i]->next->site]->conflict[locks[i]->site] = site_graph[locks[i]->site];
		}
	}
}

int rehash_conflicts(){
	int i,j;
	for(i=0;i<num_sites;i++){
		for(j=0;j<num_sites;j++){
			site_graph[i]->conflict[j] = NULL;
		}
	}	

	for(i=0;i<num_vars;i++){
		if( locks[i] != NULL ){
			struct lockq * temp = locks[i]->next;
			while( temp != NULL ){
				//fprintf(stderr,"Adding conflict var %c from site %d to site %d\n",(char)i+97,temp->site,locks[i]->site);
				site_graph[temp->site]->conflict[locks[i]->site] = site_graph[locks[i]->site];
				temp = temp->next;
			}
		}	
	}

	int conflicts_found = 1;
	srand(time(NULL));
	int rabort = rand();
	while( conflicts_found ){
		conflicts_found = 0;
		for(i=0;i<num_sites;i++){
			j = (i + rabort)%3;

			dfs_graph(site_graph[j],j);
			if( need_abort[j] && abort_sent[j] == 0){
				conflicts_found = 1;
				abort_sent[j] = 1;
				char abort[2];
				abort[0] = 'A';
				abort[1] = '\0';
				write(sitefd[j],abort, 2);
				release_all_site_locks(j);
				//break;
			}
		}
	}

}

void * release_lock(int site, int varindex){
	int j,i;
	//here we need to relases the lock, if there is another lock on the queue, we should grant it to the requestor!
	if( locks[varindex]->next == NULL ){
		fprintf(stderr,"Release of lock for site %d on var %c. No queue\n",site,vars[varindex]);
		free(locks[varindex]);
		locks[varindex] = NULL;
	}else{
		fprintf(stderr,"Release of lock for site %d on var %c. Granting on queue to site %d ",site,vars[varindex],locks[varindex]->next->site);
		struct lockq * temp = locks[varindex];
		//site_graph[temp->next->site]->conflict[temp->site] = NULL;
		locks[varindex] = locks[varindex]->next;
		free(temp);		

		//also grant here
		char grant[3];
		grant[0] = 'G';
		grant[1] = vars[varindex];
		grant[2] = '\0';		
		write(sitefd[locks[varindex]->site],grant, 3);

		/*if( locks[varindex]->next != NULL ){
			site_graph[locks[varindex]->next->site]->conflict[locks[varindex]->site] = site_graph[locks[varindex]->site];
			dfs_graph(site_graph[locks[varindex]->next->site],locks[varindex]->next->site);

			if( need_abort[locks[varindex]->next->site] ){

				char abort[2];
				abort[0] = 'A';
				abort[1] = '\0';
				//fprintf(stderr,"---------------------write the abort!\n");	
				write(sitefd[locks[varindex]->next->site],abort, 2);

				release_all_site_locks(locks[varindex]->next->site);
			}
		}*/
	}
	rehash_conflicts();
}

int visit_node(struct x_graph *node,int sindex){
	int i;

	if( node->visited == 1 ){
		fprintf(stderr,"=====================ERROR! cycle detected in graph! Node already visited! Sending abort to site %d\n",sindex);
		need_abort[sindex] = 1;
		//return 1;
	}else{
		node->visited = 1;
		for(i = 0; i< num_sites; i++ ){
			if( node->conflict[i] != NULL ){
				visit_node(node->conflict[i],sindex);
			}
		}
		node->visited = 2;
	}
	//return 0;
}

void * parse_packet(char *buffer,int sindex,int totalbytes){
	int retbytes,i;
	if( need_abort[sindex] ){
		//fprintf(stderr,"INDEX %d NEEDS ABORT\n",sindex);
		//wait and parse everything!
		int abort_index = 0;
		while( abort_index < totalbytes ){
			if( buffer[abort_index] == 'L' ){
				need_abort[sindex] = 0;
				abort_index += 2;
				abort_sent[sindex] = 0;
				break;
			}else if( buffer[abort_index] == 'R' ){
				abort_index += 5;				
			}else if( buffer[abort_index] == 'O' ){
				//this should actually just break b/c there is an issue where where all locks can be granted but 
				//the graph finds a cycle???
				//fprintf(stderr,"this is a crit error!\n");
				abort_index += 7;
			}else if( buffer[abort_index] == 'D' ){
				abort_index += 2;
			}
		}
		retbytes = abort_index;
	}else if( buffer[0] == 'L' ){
		if( need_abort[sindex] ){
			need_abort[sindex] = 0;
		}
		retbytes = 2;
	}else if( buffer[0] == 'R' ){
		int index = ((int)buffer[1]) - 97;
		fprintf(stderr,"Request %c site %d ", buffer[1],sindex);
		//lock requested, check table and set lock, respond granted
		//case q is empty
			
		struct lockq *temp = (struct lockq *)malloc(sizeof(struct lockq));
		temp->site = sindex;
		temp->next = NULL;

		//this must be gaurded
		sem_wait(&mutex);

		//case q is empty and lock can be granted immediately
		if( locks[index] == NULL ){
			fprintf(stderr,"Lock is not taken grant! site %d, var %c\n",sindex,buffer[1]);
			locks[index] = temp;
			ltails[index] = temp;
			sem_post(&mutex);
				
			char grant[3];
			grant[0] = 'G';
			grant[1] = buffer[1];
			grant[2] = '\0';			
			write(sitefd[sindex],grant, 3);


		//case q is not empty
		}else{
			if( check_hold_lock(index,sindex) ){
				//site already has a request for the lock do nothing
				//fprintf(stderr,"Case should not happen!!\n");
			}else{

				ltails[index]->next = temp;
				ltails[index] = temp;
				
				fprintf(stderr,"Lock is taken! add conflict on %d to %d \n",sindex,locks[index]->site);
				site_graph[sindex]->conflict[locks[index]->site] = site_graph[locks[index]->site];

				//check if we have a cycle here
				//dfs_graph(site_graph[sindex],sindex);
				rehash_conflicts();

			}
			sem_post(&mutex);

			/*if( need_abort[sindex] ){
				release_all_site_locks(sindex);
				sem_post(&mutex);

				char abort[2];
				abort[0] = 'A';
				abort[1] = '\0';
				//fprintf(stderr,"---------------------write the abort!\n");	
				write(sitefd[sindex],abort, 2);
			}else{
				sem_post(&mutex);
			}*/


		}

		retbytes = 5;

		/*for( i=0; i< num_vars; i++ ){
			struct lockq *t = locks[i];
			fprintf(stderr,"%d: ",i);				
			while( t != NULL ){
				fprintf(stderr,"%d ",t->site);
				t = t->next;
			}
			fprintf(stderr," -- ");
		}*/
	}else if( buffer[0] == 'O' ){
		//okay we have received a completed op from the requesting site.
		//send the update now to all other sites
		sem_wait(&mutex);
		for(i = 0; i< num_sites; i++ ){
			//fprintf(stderr,"CLEARING CONFLICTS!!!\n");
			site_graph[sindex]->conflict[i] = NULL;
			site_graph[i]->conflict[sindex] = NULL;
		}

		fprintf(stderr,"Site %d completed operation. Sending copier transaction seq %d\n",sindex,update_seq_no);
		char update_buf[11];
		update_buf[0] = 'U';
		update_buf[1] = buffer[1];
		update_buf[2] = buffer[2];
		update_buf[3] = buffer[3];
		update_buf[4] = buffer[4];
		update_buf[5] = buffer[5];
		update_buf[6] = update_seq_no;
		update_buf[7] = update_seq_no >> 8;
		update_buf[8] = update_seq_no >> 16;
		update_buf[9] = update_seq_no >> 24;
		update_buf[10] = '\0';

		for( i =0; i < num_sites; i++ ){
			//send update to all sites no in sindex
			//else it is the original requesting site, release lock.			
			//fprintf(stderr,"site fd %d\n", sitefd[j] );
			if( sitefd[i] != -1 ){					
				write(sitefd[i],update_buf,11);		
			}
		}

		char release_buf[3];
		release_buf[0] = 'K';
		release_buf[1] = buffer[1];
		release_buf[2] = '\0';
		int ret = write(sitefd[sindex],release_buf,3);
		//fprintf(stderr,"-writing to site j %d sindex %d. %s ret %d\n",j,sindex,release_buf,ret);
		//TODO release site from local lock queue...
		//sem_wait(&mutex);
		release_lock(sindex,((int)buffer[1]) - 97);
		//sem_post(&mutex);

		//fprintf(stderr,"done ops\n");
		//usleep(100000);
		update_seq_no++;
		retbytes = 7;
		sem_post(&mutex);

	}else if( buffer[0] == 'D' ){
		site_done[sindex] = 1;
		int send = 1;
		for(i=0;i<num_sites;i++){
			if( site_done[i] == 0 ){
				send = 0;
			}
		}
		if(send){
			for(i=0;i<num_sites;i++){
				char buf[2];
				buf[0] = 'D';
				buf[1] = '\0';
				write(sitefd[i],buf,2);
			}
		}
		retbytes = 2;
	}
	
	return retbytes;
}

void * read_site(void * site){
	int n,sindex,i;
	sindex = (int) site;	
	char tbuf[1500];

	sem_wait(&start_trans);

	memset(tbuf,'\0',1500);
	while(1){
		n = read(sitefd[sindex],&tbuf[0],1500);
		//fprintf(stderr,"read %d bytes");
		if( n == 0 ){
			close(sitefd[sindex]);
			sitefd[sindex] = -1;
			break;
		}

		i=0;
		while(i<n){
			//fprintf(stderr,"PP site %d index %d total %d %c\n",sindex,i,n,tbuf[i]);
			i += parse_packet(&tbuf[i],sindex,n-i);
		}
		memset(tbuf,'\0',1500);
	}
}

void error(const char *msg)
{
	perror(msg);
	exit(1);
}

int main(int argc, char *argv[])
{
	int acceptfd, portno,i,j;
	socklen_t clilen;

	struct sockaddr_in serv_addr;
	num_sites = 3;
	struct sockaddr_in site_addr[num_sites];
	int n;

	if (argc < 2) {
		fprintf(stderr,"usage %s <num vars>\n", argv[0]);
		exit(0);
	}

	num_vars = atoi(argv[1]);
	update_seq_no = 1;

	need_abort = 0;

	sem_init(&mutex, 1, 1); 
	sem_init(&start_trans, 1, 0); 

	locks = (struct lockq **)malloc(sizeof(struct lockq)*num_vars);
	ltails = (struct lockq **)malloc(sizeof(struct lockq)*num_vars);
	need_abort = (int *) malloc(sizeof(int)*num_sites);
	abort_sent = (int *) malloc(sizeof(int)*num_sites);
	site_done = (int *) malloc(sizeof(int)*num_sites);
	sitefd = (int *) malloc(sizeof(int)*num_sites);
	site_read = (pthread_t *)malloc(sizeof(pthread_t)*num_sites);

	site_graph = (struct x_graph **)malloc(sizeof(struct x_graph)*num_sites);

	for( i = 0; i < num_sites; i++ ){
		abort_sent[i] = 0;
		site_done[i] = 0;
		struct x_graph *temp = (struct x_graph *)malloc(sizeof(struct x_graph));
		temp->visited = 0;
		site_graph[i] = temp;
		site_graph[i]->conflict = (struct x_graph **)malloc(sizeof(struct x_graph)*num_sites);
		for( j = 0; j < num_sites; j++ ){
			site_graph[i]->conflict[j] = NULL;
		}
	}

	vars = (char *) malloc(num_vars);
	for(i=0; i < num_vars; i++ ){
		vars[i] = (char)(97+i);
		//printf("%c\n",vars[i]);
		locks[i] = NULL;
		ltails[i] = NULL;
	}

	acceptfd = socket(AF_INET, SOCK_STREAM, 0);
	if (acceptfd < 0){ 
        	error("ERROR opening socket");
	}
	
	bzero((char *) &serv_addr, sizeof(serv_addr));
	portno = 51234;
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portno);
	if (bind(acceptfd, (struct sockaddr *) &serv_addr, sizeof(struct sockaddr_in)) < 0){ 
              error("ERROR on binding");
	}

	listen(acceptfd,5);
	clilen = sizeof(struct sockaddr_in);



	for(i=0;i<num_sites; i++){
		sitefd[i] = accept(acceptfd,(struct sockaddr *) &site_addr[i], &clilen);
	    	if (sitefd[i] < 0) {
	    		fprintf(stderr,"ERROR on accept");
		}else{
			fprintf(stderr,"Site %d online!\n",i);
		}
		if( pthread_create( &site_read[i], NULL, &read_site, (void*) i) ){
			fprintf(stderr,"ERROR cannot create read thread for site %d!",i);
		}
	}

	for(i=0;i<num_sites; i++){
		char buf[8];
		buf[0] = 'N';
		buf[1] = num_vars;
		buf[2] = num_vars >> 8;
		buf[3] = num_vars >> 16;
		buf[4] = num_vars >> 24;
		buf[5] = '\0';
		buf[6] = (char)i + '0';
		buf[7] = '\0';

		n = write(sitefd[i],buf, 8);
		printf("N write : %d\n",n);
		sem_post(&start_trans);
	}
	
	int finished = 0;
	while( !finished ){
		finished = 1;

		for(i=0;i<num_sites;i++){
			if( sitefd[i] != -1 ){
				finished = 0;
				break;
			}
		}
		sleep(1);
	}
	fprintf(stderr,"done!\n");

	for(i=0;i<num_sites; i++){
		close(sitefd[i]);
	}
	close(acceptfd);
}

